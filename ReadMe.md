# Othello

This program allows a player to play against an AI in a game of Othello (also known as Reversi).

## Requirements

Any C++ compiler can run the program

## To Run

The software comes with a make file. Simply cd into the cloned repository and use the following command:

```
make
```


