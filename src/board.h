#ifndef BOARD_H
#define BOARD_H

#include <vector>

class move;

class board {

public:
    board();
    void makeMove(move x);
    void displayBoard();
    void placePiece(move x);
    int countAIPieces();
    int countPlayerPieces();
    char getPiece(move x);
    char getPiece(int x, int y);
    bool isMoveLegal(move x);

    // Accessors
    std::vector<std::vector<char>> getState();

private:
    // The first set of brackets is for the Y position, the second the X
    char state[8][8];

    //Helper Functions
    void flipBoard(move x);
    
    bool checkNorth(move x);
    bool checkSouth(move x);
    bool checkWest(move x);
    bool checkEast(move x);
    bool checkNorthWest(move x);
    bool checkNorthEast(move x);
    bool checkSouthWest(move x);
    bool checkSouthEast(move x);
    

};

#endif