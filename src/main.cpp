/*
*   Joseph Bauer
*   CS4200
*   October 14, 2017
*   Othello Game With a AI
*   UPDATED VERSION
*
*   Few key parts:
*    1) There is a main do while loop that serves as the game loop
*        - This loop will bust as soon the board is full
*    2) In this game loop, there is an if statement that figures out whether it is the player's or the computer's turn
*        -The variable used for this is playerTurn. True = Player's turn. False = AI's turn
*    3) A few nested if statements 
*        -The nested if for the player first determines whether the player can make a legal move or not
*            If he is able to, then it uses a do while to validate that the player inputs a legal move or not
*        -The nested if for the computer simply determines if it needs to skip the computer's turn or not
*
*   I did some major refactoring, there were a lot of things really sloppy with the way I implemented everything
*   There are now two classes. Move and Board.
*   Board objects consist of a char array that signifies the board and have a lot of funtionality built in
*   to Place pieces, display the board, determine if a move is legal, etc.
*
*   Move objects were made because a lot of the functions that I have written before took the same 4 3 inputs.
*   To fix this, I made a move class that holds the x and y positions of a move, a bool that determines who is making the 
*   move and a value that can be used to compare how "good" moves are for the AI
*
*
*/

#include <iostream>

#include "move.h"
#include "move.cpp"

#include "board.h"
#include "board.cpp"


// node Structure for building the tree to be used by min-max
struct node{
    node ** children;   // Array of nodes to hold children
    int childCount;     // Length of children array
    int value;          // Value of the current board
};

// Tree declaration
node * tree;

// FUNCTIONS
bool isGameOver(board board);
bool checkForLegalMove(board myBoard, bool x);
bool endTurn(bool x);    

//MY SUPER POWERFUL AI IMPLEMENTATION FUNCTIONS
board winDaGame(board myBoard); //This function brings all of the AI functionality together
int findAllLegalMoves(board myBoard, move list[20]);
int evaluateMove(board myBoard, move y);
node * createTree(board, int );


// MAIN BLOCK
int main(){

    //Let's do this 
    
    //Declarations
    bool gameOver   = false;
    bool legalMove;
    bool playerTurn = true;
    bool canMove    = true;

    int playerX = -1;
    int playerY = -1;

    board myBoard;
    
    move playerMove;
    
    //Game Loop
    do {

        // THIS BOARD IS FOR TESTING PURPOSES ONLY

        // move testMove;
        // testMove.setTurn(false);

        // for(int i = 0; i < 8; i++){
        //     for(int k = 0; k < 8; k++){
        //         testMove.setXPos(i);
        //         testMove.setYPos(k);

        //         myBoard.placePiece(testMove);
        //     }
        // }

        std::cout << "YOUR SCORE: " << myBoard.countPlayerPieces() << std::endl;
        std::cout << "COMP SCORE: " << myBoard.countAIPieces() << std::endl;
        std::cout << std::endl;

        myBoard.displayBoard();
        
        if(playerTurn){
        
            //This if statement will skip the player's turn if there is 
            //no legal move available to them
            if(checkForLegalMove(myBoard, true)){

                playerMove.setTurn(true);
                
                do {
                    //Get Player Input
                    //Do while loop is used to validate that the player move is legal

                    std::cout << "Enter the position that you want to place your piece: " << std::endl;
                    std::cout << "X Pos: ";
                    std::cin >> playerX;
                    std::cout << std::endl;

                    std::cout << "Y Pos: ";
                    std::cin >> playerY;
                    std::cout << std::endl;

                    playerMove.setXPos(playerX);
                    playerMove.setYPos(playerY);

                    std::cout << "You chose X: " << playerMove.getXPos() << " Y: " << playerMove.getYPos() << std::endl;
                    
                    //Function that validates whether the move is legal or not
                    legalMove = myBoard.isMoveLegal(playerMove);

                    if(!legalMove)
                        std::cout << "ILLEGAL MOVE" << std::endl;

                    std::cin.clear();
                    std::cin.ignore();
                } while (legalMove == false);

                myBoard.placePiece(playerMove);        

            } else {
                std::cout << "NO MOVE POSSIBLE, SKIPPING YOUR TURN" << std::endl;
            }
        } else {
            if(checkForLegalMove(myBoard, false)) //This ensures that the computer has a legal move
                myBoard = winDaGame(myBoard);
            else
                std::cout << "COMPUTER CANNOT MOVE SKIPPING TURN" << std::endl;
        }
        //This function swaps between the player and the computer's turn
        playerTurn = endTurn(playerTurn);

        //Function that checks to see if the myBoard is full or not
        gameOver = isGameOver(myBoard);

    } while (gameOver == false);
    
    myBoard.displayBoard();
    
    std::cout << "FINAL SCORE" << std::endl;
    std::cout << "-----------" << std::endl;
    std::cout << "YOUR SCORE: " << myBoard.countPlayerPieces() << std::endl;
    std::cout << "COMP SCORE: " << myBoard.countAIPieces() << std::endl;
    std::cout << std::endl;
    
    if(myBoard.countPlayerPieces() > myBoard.countAIPieces())
        std::cout << "YOU WON! :)" << std::endl;
    else if(myBoard.countPlayerPieces() == myBoard.countAIPieces())
        std::cout << "DRAW!" << std::endl;
    else 
       std:: cout << "YOU LOST" << std::endl;
    
    return 0;
}

//AI IMPLEMENTATION
board winDaGame(board myBoard){

    // Create the tree
    tree = createTree(myBoard, 3);

    //First, find all of the legal moves
    move list[20];
    int length = findAllLegalMoves(myBoard, list);

    //Evaluate all the moves
    for(int i = 0; i < length; i++){
        list[i].calcValue(myBoard);
    }

    //This bit will list all of the potential moves the AI can make
    std::cout << "List All of The Legal Moves for the AI" << std::endl;
    std::cout << "X   Y   Value" << std::endl;
    std::cout << "-------------" << std::endl;   
    for(int i = 0; i < length; i++){
        std::cout << list[i].getXPos() << "   " << list[i].getYPos() << "   " << list[i].getValue() << std::endl;
    }

    std::cout << std::endl;

    //Find the Move with the highest value
    int highestIndex = 0;

    for(int i = 0; i < length; i++){
        if(list[i].getValue() > list[highestIndex].getValue()){
            highestIndex = i;
        }
    }

    //Place the piece with the highest value
    std::cout << "COMPUTER CHOOSES: " << list[highestIndex].getXPos() << ", " << list[highestIndex].getYPos() << std::endl;
    std::cout << std::endl;
    myBoard.placePiece(list[highestIndex]);

    return myBoard;
}

int findAllLegalMoves(board myBoard, move list[20]){

    int z = 0;
    move x;

    x.setTurn(false);

    //Find a list of all of the legal moves that can be made by the AI
    for(int i = 0; i < 8; i++){
        for(int k = 0; k < 8; k++){

            x.setXPos(k);
            x.setYPos(i);

            if(myBoard.isMoveLegal(x)){
                list[z].setXPos(k);
                list[z].setYPos(i);
                list[z].setTurn(false);
                list[z].setValue(-1);

                z++;
            }
        }
    }

    return z;
}

// This function returns true if the game cannto continue for whatever reason
bool isGameOver(board myBoard){

    bool canPlayerMove = checkForLegalMove(myBoard, true);
    bool canComputerMove = checkForLegalMove(myBoard, false);

    if (canPlayerMove == false && canComputerMove == false){
        // If neither player can make a move, end the game
        return true;
    } else {
        // Else, keep the game going
        return false;
    }
}

bool endTurn(bool oldPlayer){
    if(oldPlayer == true)
        return false;
    else
        return true;
}

bool checkForLegalMove(board myBoard, bool playerTurn){

    int i = 0;
    int k = 0;
    move temp;

    temp.setTurn(playerTurn);

    // This loop goes through the entire game board, checking to see if 
    // Any pieces are blank
    while(i < 8){
        while(k < 8){

            temp.setYPos(i);
            temp.setXPos(k);

            if(myBoard.isMoveLegal(temp))
                return true;

            k++;
        }

        k = 0;
        i++;
    }

    return false;
}

// This function will create the tree to be searched for the best
// possible move
node * createTree(board myBoard, int depth){
    node * currentNode = new node();

    //currentNode.childCount = 
    return currentNode;
}

