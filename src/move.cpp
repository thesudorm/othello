#include <iostream>
#include "move.h"
#include "board.h"

//Constructor
move::move(){
    yPos        = -1;
    xPos        = -1;
    maxValue    = -1;
}

void move::setYPos(int x){
    this->yPos = x;
}

void move::setXPos(int x){
    this->xPos = x;
}

void move::setValue(int x){
    this->maxValue = x;
}

// If true, is human player's turn
void move::setTurn(bool y){
    this->playerTurn = y;
}

/*
    This function is for the AI
    claclValue puts a number into a move's value. The higher the number, the better the move.
*/
void move::calcValue(board myBoard){
    
    //I think passing the board should make a copy here...
    //So I think just changing this should be fine and not
    //Have any major reprucussions.. Let's find out
    myBoard.placePiece(*this);

    //Certain tiles will add a higher (or even a lower count) to
    //This count variable
    int count = 0;
    move x;

    for(int i = 0; i < 8; i++){
        for(int k = 0; k < 8; k++){

            x.setXPos(k);
            x.setYPos(i);

            if(myBoard.getPiece(x) == 'X'){
                if((i == 0 && k == 0)||(i == 7 && k == 7) || (i == 0 && k == 7) || (i == 7 && k == 0)) //These are the most important squares so we add 5
                    count = count + 5;
                else if((i == 0 && (k >= 2 && k <= 5)) || (i == 7 && (k >= 2 && k <= 5)) || (k == 0 && (i >= 2 && i <= 5)) || (k == 0 && (i >= 2 && i <= 5))) //These squares are better than normal so we add 3
                    count = count + 3;
                else if((i == 1 && (k >= 1 && k <=7)) || (i == 6 && (k >= 1 && k <=7)) || (k == 1 && (i >= 1 && i <=7)) || (k == 6 && (i >= 1 && i <=7))) //We want to stay the hell away from these so we subtract 4 from the value
                    count = count - 4;
                else //Any other square we just add 1
                    count++;
            }
        }
    }

    this->maxValue = count;
}
