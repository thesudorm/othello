#ifndef MOVE_H
#define MOVE_H

class board;

//Move Class Declaration

class move {

public:
    move();

    void setYPos(int x);
    void setXPos(int y);
    void setValue(int z);
    void setTurn(bool a);
    int getYPos(){ return yPos; }
    int getXPos(){ return xPos; }
    int getValue(){ return maxValue; }
    bool getTurn(){ return playerTurn; }
    void calcValue(board myBoard);

private:
    int yPos;  
    int xPos;
    int maxValue;       //For the AI to store eval
    bool playerTurn;    //If true, is human player's turn
};

#endif