#include <iostream>
#include "board.h"
#include "move.h"

#include <vector>

//Default constructor
board::board(){
    //Initialize the starting board state
    int k = 0;

    for(int i = 0; i < 8; i++){
        for(k = 0; k < 8; k++){
            if ((i == 3 && k == 3) || (i == 4 && k == 4))
                state[i][k] = 'O';
            else if ((i == 4 && k == 3) ||(i == 3 && k == 4))
                state[i][k] = 'X';
            else
                state[i][k] = '-';
        }
        k = 0;
    } 
}

std::vector<std::vector<char>> board::getState(){
    std::vector<std::vector<char>> toReturn;

    for(int i = 0; i++; i < 8){
        for(int k = 0; k++; k < 8){
            toReturn[i][k] == state[i][k];
        }
    }
    return toReturn;
}

//Writes the board to the screen

void board::displayBoard(){
    int k = 0;

    std::cout << "  0 1 2 3 4 5 6 7" << std::endl;

    for(int i = 0; i < 8; i++){
        std::cout << i << " ";
        for(k = 0; k < 8; k++){
            std::cout << state[i][k] << " " ;
        }

        k = 0;
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

/*
    So this function looks a lot cleaner than the previous. To get a piece, you only have to write in one object.

    HOWEVER, this makes it harder to simple just grab a piece from the board. I think the best course of action may
    be to somehow overload this function so that it takes both a move object and straight coordinates.

    Maybe even consider just hanging it so that it only takes coordinates.
*/
char board::getPiece(move x){
    return state[x.getYPos()][x.getXPos()];
}

// Overloaded constructor that takes an x and y position for the board
char board::getPiece(int x, int y){
    return state[y][x];
}
/*
    PRIVATE FUNCTION

    Only called in the place piece function.

    This is used to flip all of the pieces on a board after a move is made
*/
void board::flipBoard(move x){
    int i = 1;
    
    char currentPiece;
    char opponentPiece;

    //Check which piece is who
    if (x.getTurn()){
        currentPiece = 'O';
        opponentPiece = 'X';
    }else {
        currentPiece = 'X';
        opponentPiece = 'O';
    }

    //////////////////////////////////////////////
    if(this->checkNorth(x)){
        while(state[x.getYPos() - i][x.getXPos()] == opponentPiece){
            state[x.getYPos() - i][x.getXPos()] = currentPiece;
            i+=1;
        }
    }

    i = 1;

    if(this->checkSouth(x)){
        while(state[x.getYPos() + i][x.getXPos()] == opponentPiece){
            state[x.getYPos() + i][x.getXPos()] = currentPiece;
            i+=1;
        }
    }

    i = 1;

    if(this->checkWest(x)){
        while(state[x.getYPos()][x.getXPos() - i] == opponentPiece){
            state[x.getYPos()][x.getXPos() - i] = currentPiece;            
            i+=1;
        }
    }

    i = 1;
    
    if(this->checkEast(x)){
        while(state[x.getYPos()][x.getXPos() + i] == opponentPiece){
            state[x.getYPos()][x.getXPos() + i] = currentPiece;            
            i+=1;
        }
    }

    i = 1;

    if(this->checkNorthWest(x)){
        while(state[x.getYPos() - i][x.getXPos() - i] == opponentPiece){
            state[x.getYPos() - i][x.getXPos() - i] = currentPiece;                        
            i+=1;
        }
    }

    i = 1;

    if(this->checkNorthEast(x)){
        while(state[x.getYPos() - i][x.getXPos() + i] == opponentPiece){
            state[x.getYPos() - i][x.getXPos() + i] = currentPiece;                                    
            i+=1;
        }
    }

    i = 1;

    if(this->checkSouthEast(x)){
        while(state[x.getYPos() + i][x.getXPos() + i] == opponentPiece){
            state[x.getYPos() + i][x.getXPos() + i] = currentPiece;                                                
            i+=1;
        }
    }

    i = 1;

    if(this->checkSouthWest(x)){
        while(state[x.getYPos() + i][x.getXPos()-+ i] == opponentPiece){
            state[x.getYPos() + i][x.getXPos() - i] = currentPiece;                                    
            i+=1;
        }
    }
}

void board::placePiece(move x){
    if(x.getTurn())
        state[x.getYPos()][x.getXPos()] = 'O';
    else 
        state[x.getYPos()][x.getXPos()] = 'X';

    this->flipBoard(x);            
}

int board::countAIPieces(){
    
        int count = 0;
    
        for(int i = 0; i < 8; i++){
            for(int k = 0; k < 8; k++){
                if(state[i][k] == 'X'){
                    count++;
                }
            }
        }
    
        return count;
}

int board::countPlayerPieces(){

    int count = 0;

    for(int i = 0; i < 8; i++){
        for(int k = 0; k < 8; k++){
            if(state[i][k] == 'O'){
                count++;
            }
        }
    }

    return count;
}

bool board::isMoveLegal(move x){

    //The first thing we check for is whether or not there is already a piece
    //in the desired spot

    char currentPiece;
    char opponentPiece;

    //This swaps the variables based on whether it is the computer's turn or the
    //player's turn
    if (x.getTurn() == true){
        currentPiece = 'O';
        opponentPiece = 'X';
    }else {
        currentPiece = 'X';
        opponentPiece = 'O';
    }

    // std::cout << "PLAYER PIECE: " << currentPiece << " OPPO PIECE: " << opponentPiece << std::endl;

    // std::cout << "THIS IS BOARD BEFORE MOVE IS EVALUATED" << std::endl;
    // this->displayBoard();
    // std::cout << x.getXPos() << ", " << x.getYPos() << std::endl;

    if(this->getPiece(x) == 'O'){
        // std::cout << "THERE IS A O THERE" << std::endl;
        return false;
    }else if (this->getPiece(x) == 'X'){
        // std::cout << "THERE IS A X THERE" << std::endl;        
        return false;
    } else {

        //Thinking about calling a bunch of helper functions that will determine if a
        //move is legal or not

        return (this->checkNorth(x)     || 
                this->checkSouth(x)     || 
                this->checkWest(x)      || 
                this->checkEast(x)      || 
                this->checkNorthWest(x) || 
                this->checkNorthEast(x) || 
                this->checkSouthEast(x) || 
                this->checkSouthWest(x));
    }
}


/******************************
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
******DIRECTIONAL HELPERS******
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*******************************/
bool board::checkNorth(move x){
    int i = 1;
    
    char currentPiece;
    char opponentPiece;

    //Check which piece is who
    if (x.getTurn()){
        currentPiece = 'O';
        opponentPiece = 'X';
    }else {
        currentPiece = 'X';
        opponentPiece = 'O';
    }

    if(state[x.getYPos() - i][x.getXPos()] == opponentPiece){

        while(state[x.getYPos() - i][x.getXPos()] == opponentPiece){
            i+=1;
        }

        if(state[x.getYPos() - i][x.getXPos()] == currentPiece){
            return true;
        }else {
            return false;
        }

    } else {
        return false;
    }
}

bool board::checkSouth(move x){
    int i = 1;
    
    char currentPiece;
    char opponentPiece;

    //Check which piece is who
    if (x.getTurn()){
        currentPiece = 'O';
        opponentPiece = 'X';
    }else {
        currentPiece = 'X';
        opponentPiece = 'O';
    }

    if(state[x.getYPos() + i][x.getXPos()] == opponentPiece){

        
        while(state[x.getYPos() + i][x.getXPos()] == opponentPiece){
            i+=1;            
        }

        if(state[x.getYPos() + i][x.getXPos()] == currentPiece){
            return true;
        }else {
            return false;
        }

    } 

    return false;
}

bool board::checkWest(move x){
    int i = 1;
    
    char currentPiece;
    char opponentPiece;

    //Check which piece is who
    if (x.getTurn()){
        currentPiece = 'O';
        opponentPiece = 'X';
    }else {
        currentPiece = 'X';
        opponentPiece = 'O';
    }

    if(state[x.getYPos()][x.getXPos() - i] == opponentPiece){

        while(state[x.getYPos()][x.getXPos() - i] == opponentPiece){
            i+=1;
        }

        if(state[x.getYPos()][x.getXPos() - i] == currentPiece){
            return true;
        }else {
            return false;
        }

    } else {
        return false;
    }
}

bool board::checkEast(move x){
    int i = 1;
    
    char currentPiece;
    char opponentPiece;

    //Check which piece is who
    if (x.getTurn()){
        currentPiece = 'O';
        opponentPiece = 'X';
    }else {
        currentPiece = 'X';
        opponentPiece = 'O';
    }

    if(state[x.getYPos()][x.getXPos() + i] == opponentPiece){

        while(state[x.getYPos()][x.getXPos() + i] == opponentPiece){
            i+=1;
        }

        if(state[x.getYPos()][x.getXPos() + i] == currentPiece){
            return true;
        }else {
            return false;
        }

    } else {
        return false;
    }
}

bool board::checkNorthWest(move x){
    int i = 1;
    
    char currentPiece;
    char opponentPiece;

    //Check which piece is who
    if (x.getTurn()){
        currentPiece = 'O';
        opponentPiece = 'X';
    }else {
        currentPiece = 'X';
        opponentPiece = 'O';
    }

    if(state[x.getYPos() - i][x.getXPos() - i] == opponentPiece){

        while(state[x.getYPos() - i][x.getXPos() - i] == opponentPiece){
            i+=1;
        }

        if(state[x.getYPos() - i][x.getXPos() - i] == currentPiece){
            return true;
        }else {
            return false;
        }

    } else {
        return false;
    }
}

bool board::checkNorthEast(move x){
    int i = 1;
    
    char currentPiece;
    char opponentPiece;

    //Check which piece is who
    if (x.getTurn()){
        currentPiece = 'O';
        opponentPiece = 'X';
    }else {
        currentPiece = 'X';
        opponentPiece = 'O';
    }

    if(state[x.getYPos() - i][x.getXPos() + i] == opponentPiece){

        while(state[x.getYPos() - i][x.getXPos() + i] == opponentPiece){
            i+=1;
        }

        if(state[x.getYPos() - i][x.getXPos() + i] == currentPiece){
            return true;
        }else {
            return false;
        }

    } else {
        return false;
    }
}

bool board::checkSouthWest(move x){
    int i = 1;
    
    char currentPiece;
    char opponentPiece;

    //Check which piece is who
    if (x.getTurn()){
        currentPiece = 'O';
        opponentPiece = 'X';
    }else {
        currentPiece = 'X';
        opponentPiece = 'O';
    }

    if(state[x.getYPos() + i][x.getXPos() - i] == opponentPiece){

        while(state[x.getYPos() + i][x.getXPos() - i] == opponentPiece){
            i+=1;
        }

        if(state[x.getYPos() + i][x.getXPos() - i] == currentPiece){
            return true;
        }else {
            return false;
        }

    } else {
        return false;
    }
}

bool board::checkSouthEast(move x){
    int i = 1;
    
    char currentPiece;
    char opponentPiece;

    //Check which piece is who
    if (x.getTurn()){
        currentPiece = 'O';
        opponentPiece = 'X';
    }else {
        currentPiece = 'X';
        opponentPiece = 'O';
    }

    if(state[x.getYPos() + i][x.getXPos() + i] == opponentPiece){

        while(state[x.getYPos() + i][x.getXPos() + i] == opponentPiece){
            i+=1;
        }

        if(state[x.getYPos() + i][x.getXPos() + i] == currentPiece){
            return true;
        }else {
            return false;
        }

    } else {
        return false;
    }
}
