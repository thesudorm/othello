all:
	g++ src/main.cpp
	./a.out
	make clean

clean:
	rm a.out

test: ; g++ tests/test.cpp tests/boardtest.cpp ; ./a.out


debugtest: ; g++ tests/test.cpp tests/boardtest.cpp -g
