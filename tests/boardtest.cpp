#include "./catch2/catch.hpp"

#include "../src/board.h"
#include "../src/board.cpp"

#include "../src/move.h"
#include "../src/move.cpp"

#include <vector>
#include <iostream>

TEST_CASE("Board is set up correctly", "[Initialization]"){

    // Variables
    bool areAllOtherPiecesBlank = true;
    board myBoard; // Sets up the starting board

    // Check that both players have pieces in the correct places
    REQUIRE(myBoard.getPiece(3, 3) == myBoard.getPiece(4, 4));
    REQUIRE(myBoard.getPiece(4, 3) == myBoard.getPiece(3, 4));

    // Check that all other pieces are blank
    for(int i = 0; i < 8; i++){
        for(int k = 0; k < 8; k++){
            if((i == 3 && k == 3) || (i == 4 && k == 4) || (i == 4 && k == 3) || (i == 3 && k == 4) ){ // Exclude starting pieces
                // Do Nothing
            } else {
               if(myBoard.getPiece(k, i) != '-'){
                   areAllOtherPiecesBlank = false;
               } 
            }
        }
    }

    REQUIRE(areAllOtherPiecesBlank == true);
}

TEST_CASE("Test that pieces are flipped correctly", "[Gameplay]"){

    // Variables
    board myBoard;
    move myMove; 

    // Set up 
    myMove.setXPos(3);
    myMove.setYPos(5);
    myMove.setTurn(true);

    // Execute
    myBoard.placePiece(myMove);

    // Evaluate
    
    // First, checking to see if all player pieces are there
    REQUIRE(myBoard.getPiece(3, 3) == 'O');
    REQUIRE(myBoard.getPiece(3, 4) == 'O');
    REQUIRE(myBoard.getPiece(3, 5) == 'O');

    // Then check that the computer piece is there
    REQUIRE(myBoard.getPiece(4, 3) == 'X');
}

TEST_CASE("Test piece counting functions", "[Utility]"){

    // Variables
    board myBoard;
    int numberOfPlayerPieces;
    int numberOfComputerPieces;
    
    // Set Up
    // None

    // Execute
    numberOfPlayerPieces = myBoard.countPlayerPieces();     
    numberOfComputerPieces = myBoard.countAIPieces();     

    // Evaluate
    REQUIRE(numberOfPlayerPieces == 2); 
    REQUIRE(numberOfComputerPieces == 2); 

    // Place a piece and recount
    move myMove;
    myMove.setXPos(3);
    myMove.setYPos(5);
    myMove.setTurn(true);

    myBoard.placePiece(myMove);

    // Execute
    numberOfPlayerPieces = myBoard.countPlayerPieces();     
    numberOfComputerPieces = myBoard.countAIPieces();     

    // Evaluate
    REQUIRE(numberOfPlayerPieces == 4); 
    REQUIRE(numberOfComputerPieces == 1); 
}
